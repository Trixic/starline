var fs = require('fs');
var system = require('system');

var customerPrefix = system.env['CUSTOMER'];
var cookieFilename = customerPrefix + '/cookies.json';
var statFileName = customerPrefix + '/dailyStats.json';
var url = 'https://starline-online.ru/#login';

if (fs.isFile(cookieFilename))
  phantom.cookies = JSON.parse(fs.read(cookieFilename));

var dailyStats = [];
var failed = false;

var casper = require('casper').create({
    verbose: true,
    //logLevel: 'debug'
});

casper.options.viewportSize = { width: 1024, height: 768 };

casper.on("page.error", function(msg, trace) {
     this.echo("Error: " + msg, "ERROR");
});

function addStat() {

  //casper.capture(casper.getTitle() + '.png');
  var stat = this.fetchText('.total td');
  this.echo(stat);
  dailyStats.push(stat);
  
  this.echo('text fetched');
}

function waitWhileLoader() {

  casper.waitForSelector('.s2-loader',

    function success () {
      casper.waitWhileSelector('.s2-loader',

        function success () {
          casper.echo('loader screen is removed')
        },
        function fail () {
          casper.echo('loader screen is still shown');
          failed = true;
        },
        20000
      );
    },
    function fail () {
      casper.echo('loader screen was not shown');
    },
    1000
  );
}

function getDateString(date) {
  return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
}

function getTotalTrack(id, startDate, endDate, processTotal) {

  casper.thenEvaluate(function (id, start, end) {

    window.date1 = window.date_interval.dbegin = start;
    window.date2 = window.date_interval.dend = end;

    window.showTracksInfo(id);
  }, id, startDate, endDate);

  waitWhileLoader();

  casper.thenEvaluate(function (id) {

    window.showFullReport("html", id);
  }, id);

  casper.waitForPopup('tracks.html', function() {
    casper.echo('tracks popup loaded');
  });

  casper.then(function(){

    this.withPopup(this.popups[this.popups.length - 1], function(){

      processTotal(
        this.fetchText('#carName'),
        this.fetchText('.total td:nth-of-type(2) b').trim().split(' ')[0]
      );
      
      this.echo('text fetched');
      this.page.close();
    });
  });
}

function processCar(car) {

  casper.then(function() {
    casper.echo('id:' + car.device_id);
  });

  casper.waitFor(function() {
    var daysAgo = system.env['SCAN_DAYS_AGO'];
    var d = new Date(Date.now() - daysAgo * 24*60*60*1000);
    var begin = new Date(d.getFullYear(), d.getMonth(), d.getDate()).getTime() / 1000;
    var end = begin + 24*60*60 - 1;

    var routeUrl = "https://starline-online.ru/device/"+car.device_id+"/route?beginTime="+begin+"&endTime="+end+"&timezoneOffset=180&shortParkingTime=5&longParkingTime=60";
    var mileage = this.evaluate(function(routeUrl) {

        var js = __utils__.sendAJAX(routeUrl, 'GET', null, false);
        return JSON.parse(js).meta.mileage;

    }, routeUrl);

    var name = car.alias;
    casper.echo('car: ' + name + ' mileage: ' + mileage);

    var stat = {};
    stat.car = name;
    stat.total = (mileage / 1000).toFixed(2);
    stat.date = getDateString(d);
    dailyStats.push(stat);

    return mileage !== null;
  }, null, function timeout() {
    failed = true;
  }, 10000);
}

function login() {
//  casper.thenClick('#login');

  casper.waitForSelector('#login-form', function() {
    
    this.echo("login form filling..");
    this.fillSelectors('form#login-form', {
        'input[type="text"]': system.env['STARLINE_USER'],
        'input[type="password"]':  system.env['STARLINE_PASS']
      }, true);

    this.echo('login form filled. ' + 'user: ' + system.env['STARLINE_USER']);
  },
  function fail() {
    this.echo("login form was not loaded")
  });
}

casper.start(url);

casper.then(function() {
    this.echo('First Page: ' + this.getTitle());
});

casper.waitForSelector('#login-form',

  function success () { 
    login();
  },
  function fail () {
    this.echo("#login was not loaded");
  },
  2000
);

casper.waitForSelector('#menu',

  function success () {

    casper.wait(1000, function() {
        this.echo("I've waited for a second.");
    });

    casper.then(function() {

      this.echo("getting cars Ids..");
      var devicesUrl = "https://starline-online.ru/device?tz=180";
      var cars = this.evaluate(function(devicesUrl) {
        return  JSON.parse(__utils__.sendAJAX(devicesUrl, 'GET', null, false)).answer.devices;
      }, devicesUrl);

      this.echo('cars number: ' + cars.length);
      cars.forEach(processCar);
      //processCar(cars[0]);
    });

    casper.then(function() {
      fs.write(statFileName, JSON.stringify(dailyStats), 644);
      console.log("stats saved in file");
    });
  },
  function fail () {
    this.echo('car list was not loaded');
    failed = true;
  },
  30000
);

casper.then(function() {
    casper.capture(customerPrefix + '/final-screen.png');
    console.log("screen saved");
});

casper.then(function() {
  var cookies = JSON.stringify(phantom.cookies);
  fs.write(cookieFilename, cookies, 644);
  console.log("cookies saved");
});

casper.then(function() {
  if (failed) {
    console.log('FAIL');
    casper.exit(1);
  }
});

casper.run();
