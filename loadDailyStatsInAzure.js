var fs = require('fs');

var azure = require('azure-storage');
var tableName = process.env.TABLE_NAME;
var partitionName = 'stats'
var tableService = azure.createTableService();
var eg = azure.TableUtilities.entityGenerator;

var statFileName = process.env.CUSTOMER + '/dailyStats.json';

var stats = JSON.parse(fs.readFileSync(statFileName));

stats.forEach(function (carStat){

  var statEntity = {
    PartitionKey: eg.String(partitionName),
    RowKey: eg.String(carStat.car + '-' + carStat.date),
    car: eg.String(carStat.car),
    total: eg.Double(carStat.total),
    date: eg.String(carStat.date)
  };

  tableService.insertOrReplaceEntity(tableName, statEntity, function(error, result, response){
    if(!error) {
      console.log(carStat.car + 'entry updated');
    }
    else {
      console.log(error.message);
    }
  });
  
});
