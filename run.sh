echo ============================
date

casperjs='/root/CasperJS/node_modules/casperjs/bin/casperjs'
starline_cmd="$casperjs --ignore-ssl-errors=true /mnt/starline/starline.js"

cd /mnt/starline
cp loadDailyStatsInAzure.js /root/CasperJS/

($starline_cmd || $starline_cmd) \
  && node /root/CasperJS/loadDailyStatsInAzure.js
